<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('auth/login');
});

Auth::routes(['verify' => true]);

Route::group(['middleware' => 'auth'], function() {
    
    Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
    Route::resource('/whisper',App\Http\Controllers\WhisperController::class)->only(['index', 'create', 'store', 'show', 'edit', 'update', 'destroy']);
    Route::resource('users',App\Http\Controllers\UserController::class)->only(['index', 'show', 'edit', 'update']);
    Route::get('users/{user}/delete', [App\Http\Controllers\UserController::class, 'delete'])->name('delete');
    Route::post('users/{user}/follow', [App\Http\Controllers\UserController::class, 'follow'])->name('follow');
    Route::delete('users/{user}/unfollow', [App\Http\Controllers\UserController::class, 'unfollow'])->name('unfollow');
    Route::get('users/{user}/following', [App\Http\Controllers\UserController::class, 'following'])->name('following');
    Route::get('users/{user}/follower', [App\Http\Controllers\UserController::class, 'follower'])->name('follower');
    Route::resource('comments', App\Http\Controllers\CommentsController::class)->only(['store','edit','update','destroy']);  
    Route::get('/search', [App\Http\Controllers\SearchController::class, 'index']);
    Route::resource('favorites', App\Http\Controllers\FavoritesController::class)->only(['destroy', 'store']);

});


