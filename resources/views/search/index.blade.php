@extends('layouts.app')

@section('content')
    <div class="row justify-content-center col-12 px-5">
        <div class="card search-box col-12">
            <div class="mt-2 mb-2">
                <h2 class="text-center">
                    Search
                </h2>
            <div class="container-fluid">
                <div class="searchbtn row justify-content-around mt-3">
                    <form action="{{url('/search')}}" method="GET">
                        <p>
                            <input class="mr-5" type="text" name="keyword" value="{{$keyword}}">
                            <input type="submit" class="btn-outline-primary" value="Search">
                        </p>
                    </form>
                </div>
            </div>            
            <div class="flex-row mt-3 text-center">
                <ul class="nav nav-pills mb-3 justify-content-center" id="pills-tab" role="tablist">
                    <li class="nav-item">
                    <a class="nav-link active" id="pills-home-tab" data-toggle="pill" href="#pills-home" role="tab" aria-controls="pills-home" aria-selected="true">User</a>
                    </li>
                    <li class="nav-item">
                    <a class="nav-link" id="pills-profile-tab" data-toggle="pill" href="#pills-profile" role="tab" aria-controls="pills-profile" aria-selected="false">Post</a>
                    </li>
                </ul>
                <div class="tab-content" id="pills-tabContent">
                    <div class="tab-pane fade show active" id="pills-home" role="tabpanel" aria-labelledby="pills-home-tab">
                        @if($users->count())
                        @foreach ($users as $user)
                        <div class="card">
                            <div class="card-haeder p-3 w-100 d-flex">
                                <img src="{{ asset('storage/profile_image/' .$user->profile_image) }}" class="rounded-circle" width="50" height="50">
                                <div class="ml-2 d-flex flex-column">
                                    <a href="{{ url('users/' .$user->id) }}" class="text-secondary"><p class="mb-0">{{ $user->name }}</p><i>@</i>{{ $user->user_name }}</a>
                                </div>
                                @if (auth()->user()->isFollowed($user->id))
                                {{ csrf_field() }}
                                    <div class="px-2">
                                        <span class="px-1 bg-secondary text-light">following you</span>
                                    </div>
                                @endif    
                            </div> 
                        </div>    
                        @endforeach
                        @else 
                        <p class="text-center"><i class="fas fa-search"></i> There are no results found on your search.</p>
                        @endif
                    </div>

                    <div class="tab-pane fade" id="pills-profile" role="tabpanel" aria-labelledby="pills-profile-tab">
                        @if($whispers->count())
                        @foreach ($whispers as $timeline)
                        <div class="col-md-12 mb-3">
                            <div class="card">
                                <div class="card-haeder p-3 w-100 d-flex">
                                    <img src="{{ asset('storage/profile_image/' .$timeline->user->profile_image) }}" class="rounded-circle" width="50" height="50">
                                    <div class="ml-2 d-flex flex-column">
                                        <a href="{{ url('users/' .$timeline->user->id) }}" class="text-secondary"><p class="mb-0">{{ $timeline->user->name }}</p>
                                        <i>@</i>{{ $timeline->user->user_name }}</a>
                                    </div>
                                    <div class="d-flex justify-content-end flex-grow-1">
                                        <p class="mb-0 text-secondary">{{ $timeline->created_at->format('Y-m-d H:i') }}</p>
                                    </div>
                                </div>
                                <div class="card-body text-left">
                                    {!! nl2br(e($timeline->text)) !!}
                                </div>
                                <div class="card-footer py-1 d-flex justify-content-end bg-white">
                                    @if ($timeline->user->id === Auth::user()->id)
                                        <div class="dropdown mr-3 d-flex align-items-center">
                                            <a href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                <i class="fas fa-ellipsis-v fa-fw"></i>
                                            </a>
                                            <div class="dropdown-menu" aria-labelledby="dropdownMenuLink">
                                                <form method="POST" action="{{ url('whisper/' .$timeline->id) }}" class="mb-0">
                                                    @csrf
                                                    @method('DELETE')

                                                    <a href="{{ url('whisper/' .$timeline->id .'/edit') }}" class="dropdown-item">edit</a>
                                                    <button type="submit" class="dropdown-item del-btn">delete</button>
                                                </form>
                                            </div>
                                        </div>
                                    @endif
                                    <div class="mr-3 d-flex align-items-center">
                                        <a href="{{ url('whisper/' .$timeline->id) }}"><i class="far fa-comment fa-fw"></i></a>
                                        <p class="mb-0 text-secondary">{{ count($timeline->comments) }}</p>
                                    </div>
                                    <div class="d-flex align-items-center">
                                        @if (!in_array($user->id, array_column($timeline->favorites->toArray(), 'user_id'), TRUE))
                                            <form method="POST" action="{{ url('favorites/') }}" class="mb-0">
                                                @csrf

                                                <input type="hidden" name="whisper_id" value="{{ $timeline->id }}">
                                                <button type="submit" class="btn p-0 border-0 text-primary"><i class="far fa-heart fa-fw"></i></button>
                                            </form>
                                        @else
                                            <form method="POST" action="{{ url('favorites/' .array_column($timeline->favorites->toArray(), 'id', 'user_id')[$user->id]) }}" class="mb-0">
                                                @csrf
                                                @method('DELETE')

                                                <button type="submit" class="btn p-0 border-0 text-danger"><i class="fas fa-heart fa-fw"></i></button>
                                            </form>
                                        @endif
                                        <p class="mb-0 text-secondary">{{ count($timeline->favorites) }}</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        @endforeach
                        @else
                            <p class="text-center"><i class="fas fa-search"></i> There are no results found on your search.</p>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

