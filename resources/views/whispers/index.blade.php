@extends('layouts.app')

@section('content')
    <div class="row justify-content-center col-12 px-5">
        TIMELINE
        @if (!empty($timelines ?? ''))
            @foreach ($timelines ?? '' as $timeline)
                <div class="col-md-12 mb-3">
                    <div class="card">
                        <div class="card-haeder p-3 w-100 d-flex">
                            <img src="{{ asset('storage/profile_image/' .$timeline->user->profile_image) }}" class="rounded-circle" width="50" height="50">
                            <div class="ml-2 d-flex flex-column">
                            <a href="{{ url('users/' .$timeline->user->id) }}" class="text-secondary"><p class="mb-0">{{ $timeline->user->name }}</p>
                                <i>@</i>{{ $timeline->user->user_name }}</a>
                            </div>
                            <div class="d-flex justify-content-end flex-grow-1">
                                <p class="mb-0 text-secondary">{{ $timeline->created_at->format('Y-m-d H:i') }}</p>
                            </div>
                        </div>
                        <div class="card-body">
                            {!! nl2br(e($timeline->text)) !!}
                        </div>

                        <div class="row">
                            @foreach ($images as $image)

                            @if ($timeline->id == $image->whisper_id)
                            
                                <div class="col-md-4">
                                    <img src="{{ asset('storage/post_image/' .$image->image_file) }}" class="img-rounded m-3" width="200" height="200">
                                </div>
                            
                            @endif
                            @endforeach
                        </div>

                        <div class="card-footer py-1 d-flex justify-content-end bg-white">
                            @if ($timeline->user->id === Auth::user()->id)
                                <div class="dropdown mr-3 d-flex align-items-center">
                                    <form method="POST" action="{{ url('whisper/' .$timeline->id) }}" class="mb-0">
                                        @csrf
                                        @method('DELETE')
                                        <button type="submit" class="dropdown-item del-btn"><i class="fas fa-trash-alt"></i></button>
                                    </form>
                                </div>
                            @endif
                            <div class="mr-3 d-flex align-items-center">
                                <a href="{{ url('whisper/' .$timeline->id) }}"><i class="far fa-comment fa-fw"></i></a>
                                <p class="mb-0 text-secondary">{{ count($timeline->comments) }}</p>
                            </div>
                            <div class="d-flex align-items-center">
                                @if (!in_array($user->id, array_column($timeline->favorites->toArray(), 'user_id'), TRUE))
                                    <form method="POST" action="{{ url('favorites/') }}" class="mb-0">
                                        @csrf

                                        <input type="hidden" name="whisper_id" value="{{ $timeline->id }}">
                                        <button type="submit" class="btn p-0 border-0 text-primary"><i class="far fa-heart fa-fw"></i></button>
                                    </form>
                                @else
                                    <form method="POST" action="{{ url('favorites/' .array_column($timeline->favorites->toArray(), 'id', 'user_id')[$user->id]) }}" class="mb-0">
                                        @csrf
                                        @method('DELETE')

                                        <button type="submit" class="btn p-0 border-0 text-danger"><i class="fas fa-heart fa-fw"></i></button>
                                    </form>
                                @endif
                                <p class="mb-0 text-secondary">{{ count($timeline->favorites) }}</p>
                            </div>
                        </div>
                    </div>
                </div>
            @endforeach
        @else
        Follow someone first
        @endif
    </div>
    <div class="my-4 d-flex justify-content-center">
        {{ $timelines ?? ''->links() }}
    </div>
@endsection