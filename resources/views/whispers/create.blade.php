@extends('layouts.app')

@section('content')
    <div class="row justify-content-center col-12 px-5">
        <div class="col-md-12">
            <div class="card">
                <h2 class="card-header text-center">
                    Create post
                </h2>
                <div class="card-body">
                    <form method="POST" action="{{ route('whisper.store') }}" enctype="multipart/form-data">
                        @csrf
                        <div class="form-group row mb-0">
                            <div class="col-md-12 p-3 w-100 d-flex">
                                <img src="{{ asset('storage/profile_image/' .$user->profile_image) }}" class="rounded-circle" width="50" height="50">
                                <div class="ml-2 d-flex flex-column">
                                    <p class="mb-0">{{ $user->name }}</p>
                                    <a href="{{ url('users/' .$user->id) }}" class="text-secondary">
                                        <i>@</i>{{ $user->user_name }}
                                    </a>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <textarea class="form-control @error('text') is-invalid @enderror" placeholder="" name="text" required autocomplete="text" rows="4">{{ old('text') }}</textarea>

                                @error('text')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-12 text-right">
                            <input class="" name="image_file1" type="file" value="{{ old('image_file1') }}"><br>
                            <input class="" name="image_file2" type="file" value="{{ old('image_file2') }}"><br>
                            <input class="" name="image_file3" type="file" value="{{ old('image_file3') }}"><br>
                                <i class="fas fa-camera fa-2x"></i>
                                <p class="mb-4 text-danger">Within 140 characters</p>
                                <button type="submit" class="btn btn-outline-primary">
                                    Post
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection

