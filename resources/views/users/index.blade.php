@extends('layouts.app')

@section('content')
        <div class="row justify-content-center col-12 px-5">
        USERS
            <div class="col-md-12">
                @foreach ($all_users as $user)
                    <div class="card">
                        <div class="card-haeder p-3 w-100 d-flex">
                            <img src="{{ asset('storage/profile_image/' .$user->profile_image) }}" class="rounded-circle" width="50" height="50">
                            <div class="ml-2 d-flex flex-column">
                                <a href="{{ url('users/' .$user->id) }}" class="text-secondary"><p class="mb-0">{{ $user->name }}</p><i>@</i>{{ $user->user_name }}</a>
                            </div>
                            @if (auth()->user()->isFollowed($user->id))
                             {{ csrf_field() }}
                                <div class="px-2">
                                    <span class="px-1 bg-secondary text-light">following you</span>
                                </div>
                            @endif
                            <div class="d-flex justify-content-end flex-grow-1">
                                @if (auth()->user()->isFollowing($user->id))
                                    <form action="{{ route('unfollow', ['user' => $user->id]) }}" method="POST">
                                        {{ csrf_field() }}
                                        {{ method_field('DELETE') }}

                                        <button type="submit" class="btn btn-danger">unfollow</button>
                                    </form>
                                @else
                                    <form action="{{ route('follow', ['user' => $user->id]) }}" method="POST">
                                        {{ csrf_field() }}

                                        <button type="submit" class="btn btn-primary">follow</button>
                                    </form>
                                @endif
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
            <div class="my-4 d-flex justify-content-center">
                {{ $all_users->links() }}
            </div>
        </div>
@endsection


