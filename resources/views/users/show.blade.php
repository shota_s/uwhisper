@extends('layouts.app')

@section('content')
<div class="row justify-content-center flex-column col-12 px-5">
    <div class="text-center">
        PROFILE
    </div> 
    <div class="card user_prof">
        <div class="card-body">
            <div class="d-flex flex-row">
                <div class="mr-4">
                    <a href="#" onclick="$('#photo_upload').modal('show')">
                        <img src="{{ asset('storage/profile_image/' .$user->profile_image) }}" class="rounded-circle" style="width:100px; height: 100px">
                    </a>
                    <br>
                </div>
                <div class="row w-100">
                    <div class="col-12">
                        <h3>{{ $user->name }}</h3>
                        <p class="text-secondary"><i>@</i>{{ $user->user_name }}
                            @if ($is_followed)
                                <span class="mt-2 px-1 bg-secondary text-light">following you</span>
                            @endif
                        </p>
                        <hr>
                    </div>
                    <div class="col-md-3">
                        <a href="{{ url('users/' .$user->id .'/follower') }}">
                        <h4>{{ $fr_users->count() }}</h4>
                        <h6>Followers</h6>
                        </a>
                    </div>
                    <div class="col-md-3">
                        <a href="{{ url('users/' .$user->id .'/following') }}">
                        <h4>{{ $f_users->count() }}</h4>
                        <h6>Following</h6>
                        </a>
                    </div>
                    @if ($user->id === Auth::user()->id)
                    <div class="col-md-3">
                        <a href="{{ url('users/' .$user->id .'/edit') }}">
                        <h4><i class="fas fa-user-edit"></i></h4>
                        <h6>Edit profile</h6>    
                        </a>   
                    </div>
                    @else
                        @if ($is_following)
                            <form action="{{ route('unfollow', ['user' => $user->id]) }}" method="POST">
                                {{ csrf_field() }}
                                {{ method_field('DELETE') }}

                                <button type="submit" class="btn btn-danger">unfollow</button>
                            </form>
                        @else
                            <form action="{{ route('follow', ['user' => $user->id]) }}" method="POST">
                                {{ csrf_field() }}

                                <button type="submit" class="btn btn-primary">follow</button>
                            </form>
                        @endif
                    @endif
                </div>    
            </div>
    
            @if (isset($timelines))
            @foreach ($timelines as $timeline)
            <div class="col-md-12 mb-3 mt-3">
                <div class="card">
                    <div class="card-haeder p-3 w-100 d-flex">
                        <img src="{{ asset('storage/profile_image/' .$user->profile_image) }}" class="rounded-circle" width="50" height="50">
                        <div class="ml-2 d-flex flex-column flex-grow-1">
                            <p class="mb-0">{{ $timeline->user->name }}</p>
                            <a href="{{ url('users/' .$timeline->user->id) }}" class="text-secondary"><i>@</i>{{ $timeline->user->user_name }}</a>
                        </div>
                        <div class="d-flex justify-content-end flex-grow-1">
                            <p class="mb-0 text-secondary">{{ $timeline->created_at->format('Y-m-d H:i') }}</p>
                        </div>
                    </div>
                    <div class="card-body">
                        {{ $timeline->text }}
                    </div>
                    <div class="card-footer py-1 d-flex justify-content-end bg-white">
                        @if ($timeline->user->id === Auth::user()->id)
                            <div class="dropdown mr-3 d-flex align-items-center">
                                <a href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    <i class="fas fa-ellipsis-v fa-fw"></i>
                                </a>
                                <div class="dropdown-menu" aria-labelledby="dropdownMenuLink">
                                    <form method="POST" action="{{ url('whisper/' .$timeline->id) }}" class="mb-0">
                                        @csrf
                                        @method('DELETE')

                                        <a href="{{ url('whisper/' .$timeline->id .'/edit') }}" class="dropdown-item">edit</a>
                                        <button type="submit" class="dropdown-item del-btn">delete</button>
                                    </form>
                                </div>
                            </div>
                        @endif
                        <div class="mr-3 d-flex align-items-center">
                            <a href="{{ url('whisper/' .$timeline->id) }}"><i class="far fa-comment fa-fw"></i></a>
                            <p class="mb-0 text-secondary">{{ count($timeline->comments) }}</p>
                        </div>
                        <div class="d-flex align-items-center">
                            @if (!in_array(Auth::user()->id, array_column($timeline->favorites->toArray(), 'user_id'), TRUE))
                                <form method="POST" action="{{ url('favorites/') }}" class="mb-0">
                                    @csrf

                                    <input type="hidden" name="whisper_id" value="{{ $timeline->id }}">
                                    <button type="submit" class="btn p-0 border-0 text-primary"><i class="far fa-heart fa-fw"></i></button>
                                </form>
                            @else
                                <form method="POST"action="{{ url('favorites/' .array_column($timeline->favorites->toArray(), 'id', 'user_id')[Auth::user()->id]) }}" class="mb-0">
                                    @csrf
                                    @method('DELETE')

                                    <button type="submit" class="btn p-0 border-0 text-danger"><i class="fas fa-heart fa-fw"></i></button>
                                </form>
                            @endif
                            <p class="mb-0 text-secondary">{{ count($timeline->favorites) }}</p>
                        </div>
                    </div>
                </div>
            </div>
            @endforeach
            @endif
        </div>            
    </div>
</div>
@endsection






