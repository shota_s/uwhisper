
@extends('layouts.app')

@guest

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-body">
                    <form class="mt-5, signupForm" id="new_user" action="{{ route('login') }}" accept-charset="UTF-8" method="post">
                        {{ csrf_field() }}
                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                            <label for="user_email">EMAIL ADDRESS</label>
                            <input class="form-control" placeholder="xxxxxxx@gmail.com" autocomplete="email" type="email" name="email" value="{{ old('email') }}" required>
                            @if ($errors->has('email'))
                                <span class="help-block">
                                <strong>{{ $errors->first('email') }}</strong>
                                </span>
                            @endif
                        </div>
                        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                            <label for="user_password">PASSWORD</label>
                            <em>(8 or more characters)</em>
                            <br>
                            <input class="form-control" placeholder="8 or more characters" autocomplete="off" type="password" name="password" required>
                            @if ($errors->has('password'))
                                <span class="help-block">
                                <strong>{{ $errors->first('password') }}</strong>
                                </span>
                            @endif
                        </div>
                        <!-- <div class="form-group row">
                            <div class="col-md-6 offset-md-4">
                                <div class="form-check">
                                    <input class="form-check-input" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>

                                    <label class="form-check-label" for="remember">
                                        {{ __('Remember Me') }}
                                    </label>
                                </div>
                            </div>
                        </div> -->
                        <div class="form-group row mb-0">
                            <div class="col-md-8 offset-md-4">
                                <button type="submit" class="btn btn-outline-primary">
                                    {{ __('Sign in') }}
                                </button>

                                @if (Route::has('password.request'))
                                    <a class="btn btn-link" href="{{ route('password.request') }}">
                                        {{ __('Forgot Your Password?') }}
                                    </a>
                                @endif
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@else

@section('content')

<h4 class="text-center mt-3">Welcome to User whisper.</h4>

<h4 class="text-center mt-3">Please select a page from the list above！</h4>

<div class="text-center">
<i class="fas fa-glass-cheers fa-7x mt-5"></i>
</div>
@endsection

@endguest