@extends('layouts.verify')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <h2 class="card-header font-weight-bold">{{ __('sent a message to your email address!') }}</h2>

                <div class="card-body">
                    @if (session('resent'))
                        <div class="alert alert-success" role="alert">
                            {{ __('A fresh verification link has been sent to your email address.') }}
                        </div>
                    @endif

                    {{ __('Before proceeding, please check your email for a verification link.') }}
                    {{ __('If you did not receive the email') }},
                    <form class="d-inline" method="POST" action="{{ route('verification.resend') }}">
                        @csrf
                        <button type="submit" class="btn btn-link p-0 m-0 align-baseline">{{ __('click here to request another') }}</button>.
                    </form>
                </div>
            </div>
            <div class="mt-5 mx-auto">
                <div class="mr-5 pr-5 text-center"><i class="far fa-paper-plane fa-5x fa-rotate-90 "></i></div>
                <div class="mt-3 text-center"><i class="fas fa-envelope fa-7x ml-5 "></i></div>
            </div>
        </div>
    </div>
</div>
@endsection
