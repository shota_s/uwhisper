<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

use App\Models\User;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for ($i = 1; $i <= 10; $i++) {
            DB::table('users')->insert([
                'name'    => 'a' .$i,
                'user_name'           => '@TEST' .$i,
                'profile_image'  => '3kjxVvIjsS3U9E5BXwerIzebVX6UooElAMriw7sb.jpg',
                'email'          => 'a' .$i .'@a.com',
                'password'       => Hash::make('12345678'),
                'created_at'     => now(),
                'updated_at'     => now()
            ]);
        }
    }
}