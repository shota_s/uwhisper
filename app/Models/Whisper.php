<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class Whisper extends Model
{
    use SoftDeletes;

    protected $table = 'whispers';
    protected $dates = ['deleted_at'];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'text'
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function favorites()
    {
        return $this->hasMany(Favorite::class);
    }

    public function comments()
    {
        return $this->hasMany(Comment::class);
    }


    public function image()
    {
        return $this->hasMany(Image::class);
    }


    public function getUserTimeLine(Int $user_id)
    {
        return $this->where('user_id', $user_id)->orderBy('created_at', 'DESC')->paginate(50);
    }

    public function getTimeLines(Int $user_id, Array $follow_ids)
    {
        $follow_ids[] = $user_id;
        return $this->whereIn('user_id', $follow_ids)->orderBy('created_at', 'DESC')->paginate(50);
    }

    public function getWhisperCount(Int $user_id)
    {
        return $this->where('user_id', $user_id)->count();
    }
    public function whisperStore(Int $user_id, Array $data)
    {
        $this->user_id = $user_id;
        $this->text = $data['text'];
        $this->save();

        return;
    }

    public function getEditWhisper(Int $user_id, Int $whisper_id)
    {
        return $this->where('user_id', $user_id)->where('id', $whisper_id)->first();
    }

    public function whisperUpdate(Int $whisper_id, Array $data)
    {
        $this->id = $whisper_id;
        $this->text = $data['text'];
        $this->update();

        return;
    }

    public function whisperDestroy(Int $user_id, Int $whisper_id)
    {
        return $this->where('user_id', $user_id)->where('id', $whisper_id)->delete();
    }

    public function getWhisper(Int $whisper_id)
    {
        return $this->with('user')->where('id', $whisper_id)->first();
    }
}
