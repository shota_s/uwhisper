<?php

namespace App\Models;


use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Image extends Model
{

  public function whisper()
    {
        return $this->belongsTo(Whisper::class);
    }

  use SoftDeletes;

  protected $table = 'images';
  protected $dates = ['deleted_at'];

  protected $fillable = [
    'whisper_id'
];

}
