<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Favorite extends Model
{
    public $timestamps = false;

  
    public function isFavorite(Int $user_id, Int $whisper_id) 
    {
        return (boolean) $this->where('user_id', $user_id)->where('whisper_id', $whisper_id)->first();
    }

    public function storeFavorite(Int $user_id, Int $whisper_id)
    {
        $this->user_id = $user_id;
        $this->whisper_id = $whisper_id;
        $this->save();

        return;
    }

    public function destroyFavorite(Int $favorite_id)
    {
        return $this->where('id', $favorite_id)->delete();
    }
}