<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;
use App\Models\User;
use App\Models\Whisper;
use App\Models\Follower;
use App\Models\Image;
use Illuminate\Pagination\Paginator;


class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('verified');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
   

    public function index(Whisper $whisper, Follower $follower)
    { 
        $user = auth()->user();
        $follow_ids = $follower->followingIds($user->id);


        $following_ids = $follow_ids->pluck('followed_id')->toArray();
        $images = Image::get();


        $timelines = $whisper->getTimelines($user->id, $following_ids);

      

        return view('home', [
            'user'      => $user,
            'timelines' => $timelines,
            'images'    => $images,
        ]);
    } 

 }

        
