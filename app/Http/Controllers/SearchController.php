<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;
use App\Models\User;
use App\Models\Whisper;
use App\Models\Follower;
use Illuminate\Pagination\Paginator;
use Illuminate\Database\Eloquent\SoftDeletes;

class SearchController extends Controller
{
    use SoftDeletes;
    //
    public function index(Whisper $whisper, Request $request, Follower $follower)
    {
        $user = auth()->user();
        $keyword = $request->input('keyword');
        $follow_ids = $follower->followingIds($user->id);
        $following_ids = $follow_ids->pluck('followed_id')->toArray();
        $timelines = $whisper->getTimelines($user->id, $following_ids);
        
        $query = Whisper::query();
        $query2 = User::query();
 
        if (!empty($keyword)) {
            $query->where('text', 'LIKE', "%{$keyword}%");
        }
        if (!empty($keyword)) {
            $query2->where('name', 'LIKE', "%{$keyword}%");
        }
 
        $whispers = $query->get();
        $users = $query2->get();
 
        return view('search.index', compact('whispers', 'keyword', 'timelines','user','users'));
    }
}
