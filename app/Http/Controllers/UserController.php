<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;
use App\Models\User;
use App\Models\Whisper;
use App\Models\Follower;
use Illuminate\Pagination\Paginator;
use Illuminate\Database\Eloquent\SoftDeletes;


class UserController extends Controller
{
    //
    use SoftDeletes;

    public function index(User $user)
    {
        $all_users = $user->getAllUsers(auth()->user()->id);
        $users = User::all();
    
        return view('users.index', [
            'all_users'  => $all_users,
            'users'      => $users
        ]);

    }

    public function show(User $user, Whisper $whisper, Follower $follower)
    {
        $login_user = auth()->user();
        $is_following = $login_user->isFollowing($user->id);
        $is_followed = $login_user->isFollowed($user->id);
        $timelines = $whisper->getUserTimeLine($user->id);
        $whisper_count = $whisper->getWhisperCount($user->id);
        $follow_count = $follower->getFollowCount($user->id);
        $follower_count = $follower->getFollowerCount($user->id);    

        $follow_ids = $follower->followingIds($user->id);
        $follower_ids = $follow_ids->pluck('followed_id')->toArray();
        $fr_users = $user->getFollowers($user->id, $follower_ids);
        $f_users = $user->getFollows($user->id, $follower_ids);
        

        return view('users.show', [
            'user'           => $user,
            'is_following'   => $is_following,
            'is_followed'    => $is_followed,
            'timelines'      => $timelines,
            'whisper_count'    => $whisper_count,
            'follow_count'   => $follow_count,
            'follower_count' => $follower_count,
            'f_users' => $f_users,
            'fr_users' => $fr_users
        ]);
    }

    public function edit(User $user)
    {
        return view('users.edit', ['user' => $user]);
    }

    public function update(Request $request, User $user)
    {
        $data = $request->all();
        $validator = Validator::make($data, [
            'user_name'   => ['required', 'string', 'max:50', Rule::unique('users')->ignore($user->id)],
            'name'          => ['required', 'string', 'max:255'],
            'profile_image' => ['file', 'image', 'mimes:jpeg,png,jpg,JPG,PNG,JPEG', 'max:2048'],
            'email'         => ['required', 'string', 'email', 'max:255', Rule::unique('users')->ignore($user->id)]
        ]);
        $validator->validate();
        $user->updateProfile($data);

        return redirect('users/'.$user->id);
    }

    public function follow(User $user)
    {
     
        $follower = auth()->user();
        // フォローしているか
        $is_following = $follower->isFollowing($user->id);
        if(!$is_following) {
            // フォローしていなければフォローする
            $follower->follow($user->id);
            return back();
        }
    }

    // フォロー解除
    public function unfollow(User $user)
    {
       
        $follower = auth()->user();
        // フォローしているか
        $is_following = $follower->isFollowing($user->id);
        if($is_following) {
            // フォローしていればフォローを解除する
            $follower->unfollow($user->id);
            return back();
        }
    }

    public function getEditWhisper(Int $user_id, Int $whisper_id)
    {
        return $this->where('user_id', $user_id)->where('id', $whisper_id)->first();
    }

   
    public function follower(User $user, Follower $follower)
    {
       
        $follow_ids = $follower->followingIds($user->id);
   
        $follower_ids = $follow_ids->pluck('followed_id')->toArray();

        $f_users = $user->getFollowers($user->id, $follower_ids);

        return view('common', [
            'user'      => $user,
            'f_users' => $f_users
        ]);
    } 

    public function following(User $user, Follower $follower)
    {
        
        $follow_ids = $follower->followingIds($user->id);
   
        $following_ids = $follow_ids->pluck('followed_id')->toArray();

        $f_users = $user->getFollows($user->id, $following_ids);

        return view('common', [
            'user'      => $user,
            'f_users' => $f_users
        ]);
    } 
       
    public function delete($id)
    {
        User::find($id)->delete(); // softDelete
 
        return redirect()->to('/');
    }


   
}

