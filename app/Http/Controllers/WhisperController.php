<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Models\Whisper;
use App\Models\Comment;
use App\Models\User;
use App\Models\Follower;
use App\Models\Image;

class WhisperController extends Controller
{
    //
    public function index(Whisper $whisper, Follower $follower, Image $image)
    {
        $user = auth()->user();
        $follow_ids = $follower->followingIds($user->id);


        $following_ids = $follow_ids->pluck('followed_id')->toArray();
        $images = Image::get();


        $timelines = $whisper->getTimelines($user->id, $following_ids);

      

        return view('whispers.index', [
            'user'      => $user,
            'timelines' => $timelines,
            'images'    => $images,
        ]);
    }
    
    public function create()
    {
        $user = auth()->user();

        return view('whispers.create', [
            'user' => $user
        ]);
    }

    public function store(Request $request, Whisper $whisper)
    {
        $user = auth()->user();

        $data = $request->all();
        

        $validator = Validator::make($data, [
            'text' => ['required', 'string', 'max:140'],
        ]);

        $validator->validate();
        $whisper->whisperStore($user->id, $data);

        if ($request->hasFile('image_file1')) {

        $image = new Image;
        $filename = $request->file('image_file1')->store('public/post_image'); // publicフォルダに保存
        $image->image_file = str_replace('public/post_image/','',$filename); // 保存するファイル名からpublicを除外
        $image->whisper_id=$whisper->id;
        $image->save();
        
        }

        if ($request->hasFile('image_file2')) {

            $image = new Image;
            $filename = $request->file('image_file2')->store('public/post_image'); // publicフォルダに保存
            $image->image_file = str_replace('public/post_image/','',$filename); // 保存するファイル名からpublicを除外
            $image->whisper_id=$whisper->id;
            $image->save();
            
        }
        
        if ($request->hasFile('image_file3')) {

            $image = new Image;
            $filename = $request->file('image_file3')->store('public/post_image'); // publicフォルダに保存
            $image->image_file = str_replace('public/post_image/','',$filename); // 保存するファイル名からpublicを除外
            $image->whisper_id=$whisper->id;
            $image->save();
            
        }


        return redirect('whisper');
    }

    public function edit(Whisper $whisper)
    {
        $user = auth()->user();
        $whispers = $whisper->getEditWhisper($user->id, $whisper->id);

        if (!isset($whispers)) {
            return redirect('whispers');
        }

        return view('whispers.edit', [
            'user'   => $user,
            'whispers' => $whispers
        ]);
    }

    public function update(Request $request, Whisper $whisper)
    {
        $data = $request->all();
        $validator = Validator::make($data, [
            'text' => ['required', 'string', 'max:140']
        ]);

        $validator->validate();
        $whisper->whisperUpdate($whisper->id, $data);
       

        return redirect('users/'.$whisper->user_id);
    }

    public function destroy(Whisper $whisper)
    {
        $user = auth()->user();
        $whisper->whisperDestroy($user->id, $whisper->id);

        return back();
    }

    public function show(Whisper $whisper, Comment $comment)
    {
        $user = auth()->user();
        $whisper = $whisper->getWhisper($whisper->id);
        $comments = $comment->getComments($whisper->id);

        return view('whispers.show', [
            'user'     => $user,
            'whisper' => $whisper,
            'comments' => $comments
        ]);
    }


}
